// memanggil module http, module http sudah ada ketika instal node sehingga tidak perlu menambahkan lewat npm
var http = require('http');

// membuat object server
var serverNode = http.createServer(function (req, res) {
    // menulis head dengan type html, 200 kode respon bagus
    res.writeHead(200, {'Content-Type': 'text/html'});
    // write body
    // tulis kode html
    res.write('Hello <b>World</b>!');
    // ending respon
    res.end();
});

// menentukan port server
serverNode.listen(8800);

// menampilkan pesan ke console
console.log("server berjalan pada http://localhost:8800");
console.log("Berhasil terhubung ke server")