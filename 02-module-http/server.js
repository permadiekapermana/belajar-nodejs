var http = require('http');

var server = http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/html'});

    // tipe2 konten di head
    // untuk JSON
    // response.writeHead(200, {'Content-Type': 'application/json'});

    // // untuk PDF
    // response.writeHead(200, {'Content-Type': 'application/pdf'});

    // // untuk XML
    // response.writeHead(200, {'Content-Type': 'application/xml'});

    // mengambil url
    // response.write('URL: ' + request.url);

    // switch case url
    switch(request.url){
        case '/dashboard':
            response.write("Ini adalah halaman dashboard");
            break;
        case '/employee':
            response.write("Ini adalah halaman employee");
            break;
        case '/uncomplete':
            response.write("ini adalah halaman uncomplete");
            break;
        case '/leave':
            response.write("ini adalah halaman leave");
            break;
        default: 
            response.write("404: Page not found!");
    }

    response.end();
}).listen(8800);

console.log("server running on http://localhost:8800");