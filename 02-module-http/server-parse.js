// module http
var http = require('http');
var url = require('url');

// create server
var server = http.createServer(function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    
    // mengambil query string dari request
    var q = url.parse(request.url, true).query;
    // string harga + value harga
    // contoh ?harga=10000
    var txt = 'Harga: ' + q.harga;
    // tampilkan txt
    response.end(txt);
}).listen(8800);

console.log("Server running on http://localhost:8800");