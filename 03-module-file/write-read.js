var fs = require('fs');

fs.open('text3.txt', 'w+', function (err, file) {
    if (err) throw err;
    
    // kontent yang akan kita tulis ke file
    let content = "Hello World!";

    // tulis konten ke file
    fs.writeFile(file, content, (err) => {
        if (err) throw err;
        console.log('Success!');
    }); 

    // baca file
    fs.readFile(file, (err, data) => {
        if (err) throw err;
        console.log(data.toString('UTF8'));
    });
});