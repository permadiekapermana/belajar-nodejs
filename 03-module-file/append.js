var fs = require('fs');

// membuat file text.txt dengan isi text Hello World!
// jika di eksekusi lebih dari 1x maka isi content file akan terus bertambah
// ada 3 parameter, nama file, isi content file, function yang dijalankan
fs.appendFile('text.txt', 'Hello World!', function (err) {
  if (err) throw err;
  console.log('Success!');
});