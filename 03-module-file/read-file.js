var fs = require('fs');
var http = require('http');

var server = http.createServer(function (request, response) {

    // emthod read file
    fs.readFile('index2.html', (err, data) => {
        // jika error maka throw error
        if (err) throw err;
        
        // send response
        response.writeHead(200, {'Content-Type': 'text/html'});
        // read index.html
        response.write(data);
        response.end();
    });

}).listen(8800);

console.log("server running on http://localhost:8800");