// import module url
var url = require('url');
// var address bar
var address = 'https://localhost/search.php?year=2021&month=february';
// jika parse url di isi true maka akan jadi object
var q = url.parse(address, true);

//hasil parse URL
// htt protocol
console.log("protocol: " + q.protocol);
// host
console.log("hostname: " + q.host);
console.log("pathname: " + q.pathname);
console.log("params: " + q.search);

// ambil query string sebagai objek
var qdata = q.query;
console.log(qdata);