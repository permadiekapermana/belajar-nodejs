var http = require('http');
var url = require('url');
var fs = require('fs');

http.createServer(function (req, res) {
    var q = url.parse(req.url, true);
    var filename = "." + q.pathname;

    // read file function dari fs
    fs.readFile(filename, function(err, data) {
        if (err) {
            // error handling jika file tidak ketemu
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        } 
        // response isi file static
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
    });
  
}).listen(8800);

console.log('server is running on http://localhost:8800');