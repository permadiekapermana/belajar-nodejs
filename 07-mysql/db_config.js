// module mysql
var mysql = require('mysql');

// create connection
var db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "test_node"
});

// function check koneksi ke db
// db.connect(function(err) {
//     if (err) throw err;
//     console.log("Connected to db!");
// });

// export var db
module.exports = db;