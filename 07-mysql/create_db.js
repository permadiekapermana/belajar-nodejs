// include db_config
var db = require("./db_config");

// function connect
// buka koneksi
db.connect(function(err) {
    // jika error, thow err
    if (err) throw err;
    
    // buat db dfn nama test_node
    let sql = "CREATE DATABASE test_node";
    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Database successfully created!");
    });
});