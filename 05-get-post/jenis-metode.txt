Secara umum, ada dua metode pengiriman data pada form:

Metode GET;
dan metode POST.

Apa perbedaan metode GET dengan POST?

Metode GET akan mengirim data melalui URL, sedangkan metode POST akan mengirim data melalu latar belakang.

Metode GET cocok digunakan untuk pencarian, karena dia mengirim data melalui URL.

Sedangkan metode POST, cocoknya digunakan pada form login.