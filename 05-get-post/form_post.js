// import module http
var http = require('http');
// import module querystring
var qs = require('querystring');
// import module file system
var fs = require('fs');

// buat webserver
http.createServer(function (req, res) {

    // jika ketemu url login dan method GET
    if(req.url === "/login/" && req.method === "GET"){
        // tampilkan form login
        fs.readFile("login_form.html", (err, data) => {
            if (err) { // kirim balasan error
                res.writeHead(404, {'Content-Type': 'text/html'});
                return res.end("404 Not Found");
            } 
            // kirim form login_form.html
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    }

    // jika ketemu url login dan method GET
    if(req.url === "/login/" && req.method === "POST"){
        // ambil data dari form dan proses
        var requestBody = '';
        req.on('data', function(data) {
            // tangkap data dari form
            requestBody += data;

            // kirim balasan jika datanya terlalu besar
            // error handling jika data terlalu besar
            if(requestBody.length > 1e7) {
              res.writeHead(413, 'Request Entity Too Large', {'Content-Type': 'text/html'});
              res.end('<!doctype html><html><head><title>413</title></head><body>413: Request Entity Too Large</body></html>');
            }
        });

        // kita sudah dapat datanya
        // langkah berikutnya tinggal di-parse
        req.on('end', function() {
            var formData = qs.parse(requestBody);

            // cek login
            if( formData.username === "permadi" && formData.password === "permadi"){
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write('<h2>Selamat datang!</h2> ');
                res.write('<p>username: '+formData.username+'</p>');
                res.write('<p>password: '+formData.password+'</p>');
                res.write("<a href='/login/'>kembali</a>");
                res.end();
            // jika login gagal
            } else {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write('<h2>Login Gagal!</h2> ');
                res.write("<a href='/login/'>coba lagi</a>");
                res.end();
            }
            
          });
    }
  
}).listen(8800);

console.log('server is running on http://localhost:8800');