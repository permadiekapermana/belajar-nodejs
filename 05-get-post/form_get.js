// import module http
var http = require('http');
// import module url
var url = require('url');
// import module file system
var fs = require('fs');

// create server
http.createServer(function (req, res) {
    // jika url true
    var q = url.parse(req.url, true);

    // jika url = localhost:8800/search/ dan method form = GET
    if(q.pathname == "/search/" && req.method === "GET"){
        // ambil parameter dari URL
        var keyword = q.query.keyword;
        
        if( keyword ){
            // Ambil data dari form dengan method GET
            res.writeHead(200, {'Content-Type': 'text/html'});            
            res.write("<h3>Search Results:</h3>");
            res.write("<p>Anda mencari: <b>" + keyword + "</b></p>");
            res.end("<a href='/search/'>Kembali</a>");
        } else {
            // jika kolom input null
            if( keyword === "" ){
            res.writeHead(404, {'Content-Type': 'text/html'});
            res.write("<h3>Anda belum mengisi kolom.</h3>");
            res.write("<a href='/search/'>Kembali</a><br>");
            return res.end("404 Not Found");
            } else {
                // tampilkan form search
                fs.readFile('search.html', (err, data) => {
                    if (err) { // kirim balasan error
                        res.writeHead(404, {'Content-Type': 'text/html'});
                        return res.end("404 Not Found");
                    }
                    // kirim form search.html
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write(data);
                    return res.end();
                });
            }
        }
    }

  
}).listen(8800);

console.log('server is running on http://localhost:8800');